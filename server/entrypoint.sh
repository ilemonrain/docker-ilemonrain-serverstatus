#!/bin/bash
# 启动服务端
/usr/sbin/sergate_start
# 启动Apache2
/usr/sbin/httpd
# 卡住shell，防止自动退出 (实验性功能)
while true; do sleep 60; done;
