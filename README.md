## ServerStatus-CN on Docker —— 探针Docker化的一次尝试
( Fork from GitHub Project [cppla/ServerStatus](https://github.com/cppla/ServerStatus) )  

### 1. 镜像说明
**镜像仍在开发中！不建议大量用在生产环境！**  
  
镜像分为服务端(Server)和客户端(Client)两部分，分别对应以下镜像：  
 > **Server**：```ilemonrain/serverstatus:server```、```ilemonrain/serverstatus```、```ilemonrain/serverstatus:latest```  
 > **Client**：请参考下面的客户端配置部分

请在拉取镜像时注意区分！Server端和Client端是不一样的！互相之间不通用！  
  
 - 因为Server端尝试在Alpine Linux上直接编译失败，所以临时使用CentOS代替，稍后会找时间进行交叉编译！
 - Client端其实就是个Python脚本文件（被某位dalao忽悠了），所以弃掉Docker！
   
### 2. 部署方法
服务端部署命令行：  
```
docker run -d -p 80:80 -p 35601:35601 -v /var/ServerStatus-Docker/config:/ServerStatus/config --name ServerStatus ilemonrain/serverstatus:server
```
参数说明：  
 > **-d**：以后台模式(Deamon)启动镜像  
 > **-p 80:80**：Apache2使用的端口，如需修改端口，请修改冒号前面的端口，如 -p 8800:80  
 > **-p 35601:35601**：ServerStatus的通信端口，勿动！  
 > **-v folderout:folderin**：指定配置文件在宿主机的映射位置，上述命令行中，为将容器内的/ServerStatus/config目录，映射到宿主机的/var/ServerStatus-Docker/config目录下，这样通过编辑/var/ServerStatus-Docker/config/config.json文件即可修改ServerStatus的配置文件  
 > **--name ServerStatus**：Docker容器的名称，建议设置，方便以后的操作
 > **ilemonrain/serverstatus:server**：启动用到的镜像，服务端请用server，客户端请用client  
   
客户端部署命令行：（参考下面的客户端配置部分）  

### 3. 控制ServerStatus启动与停止
使用以下命令行控制服务端：  
  
启动ServerStatus命令行：
```
docker exec ServerStatus sergate_start
```
  
停止ServerStatus命令行：
```
docker exec ServerStatus sergate_stop
```

重新启动ServerStatus命令行：
```
docker exec ServerStatus sergate_stop && sergate_start
```

### 4. 服务端配置文件

请参阅GitHub项目 https://github.com/cppla/ServerStatus

### 5. 客户端配置
在客户端所在宿主机上，执行以下命令，下载客户端脚本到你的宿主机上：  
  
Python版本（推荐用于Linux）：
```
wget -O /usr/sbin/serverstatus-client.py https://raw.githubusercontent.com/cppla/ServerStatus/master/clients/client-linux.py  
```
  
Python-psutil版本（Linux上无法使用Python版本，以及非Linux系统使用）：
```
wget -O /usr/sbin/serverstatus-client.py  https://raw.githubusercontent.com/cppla/ServerStatus/master/clients/client-psutil.py  
```
 
之后修改你的脚本，将里面的内容按照以下格式修改：
```
SERVER = "127.0.0.1"
PORT = 35601
USER = "s01"
PASSWORD = "USER_DEFAULT_PASSWORD"
INTERVAL = 1 #更新间隔
```

参数说明：  
 > **SERVER**：你的服务端IP    
 > **PORT**：你的服务器端口（请保持默认35601）    
 > **USER**：连接用户名    
 > **PASSWORD**：连接密码 （用户名和密码请保持和服务器一致）    
 > **INTERVAL**：上传数据的间隔时间（单位：秒）  
  
改动完成并保存后，执行命令，启动客户端：
```
python /usr/sbin/serverstatus-client.py
```
  
然后回到服务器WebUI，确认是否得到客户端数据。 
  
### 6. 联系作者
E-mail: ilemonrain#ilemonrain.com